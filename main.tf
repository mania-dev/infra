variable "scaleway_access_key" {}
variable "scaleway_secret_key" {}
variable "scaleway_project_id" {}
variable "scaleway_db_user" {}
variable "scaleway_db_password" {}

module "mania-dev" {
  source              = "./mania-dev"
  scaleway_access_key = var.scaleway_access_key
  scaleway_secret_key = var.scaleway_secret_key
  scaleway_project_id = var.scaleway_project_id
  scaleway_db_user = var.scaleway_db_user
  scaleway_db_password = var.scaleway_db_password
}
