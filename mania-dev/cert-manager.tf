resource "helm_release" "cert_manager" {
  name             = "cert-manager"
  create_namespace = true
  namespace        = "cert-manager"
  chart            = "cert-manager"
  repository       = "https://charts.jetstack.io"
  version          = "v1.7.1"
  values           = [
    file("mania-dev/cert-manager.values.yaml")
  ]
}

resource "kubernetes_manifest" "prod-issuer" {
  depends_on = [helm_release.cert_manager]
  provider = kubernetes

  manifest = {
    "apiVersion" = "cert-manager.io/v1"
    "kind" = "ClusterIssuer"
    "metadata" = {
      "name" = "letsencrypt-prod"
    }
    "spec" = {
      "acme" = {
        "email" = "bossutsylvain@yahoo.fr"
        "privateKeySecretRef" = {
          "name" = "letsencrypt-prod"
        }
        "server" = "https://acme-v02.api.letsencrypt.org/directory"
        "solvers" = [
          {
            http01 = {
              "ingress" = {
                "class" = "nginx"
              }
            }
          },
        ]
      }
    }
  }
}
