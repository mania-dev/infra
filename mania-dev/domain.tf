resource "scaleway_domain_record" "db" {
  dns_zone = "db.mania-dev.com"
  name     = "db"
  type     = "A"
  data     = scaleway_rdb_instance.db.endpoint_ip
  ttl      = 3600
}


resource "scaleway_domain_record" "root" {
  dns_zone = "mania-dev.com"
  name     = "www"
  type     = "A"
  data     = scaleway_lb_ip.nginx_ip.ip_address
  ttl      = 3600
}
