variable "scaleway_access_key" {}
variable "scaleway_secret_key" {}
variable "scaleway_project_id" {}
variable "scaleway_db_user" {}
variable "scaleway_db_password" {}

variable "domain" {
  default = "mania-dev.com"
}
