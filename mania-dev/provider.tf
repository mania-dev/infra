terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      version = "2.2.1-rc.1"
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.2.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.7.1"
    }
    null = {
      source = "hashicorp/null"
      version = "3.1.0"
    }
  }
  required_version = "1.1.5"
}

provider "scaleway" {
  zone   = "fr-par-1"
  region = "fr-par"
  access_key = var.scaleway_access_key
  secret_key = var.scaleway_secret_key
  project_id = var.scaleway_project_id
}

provider "helm" {
  kubernetes {
    host = null_resource.kubeconfig.triggers.host
    token = null_resource.kubeconfig.triggers.token
    cluster_ca_certificate = base64decode(
    null_resource.kubeconfig.triggers.cluster_ca_certificate
    )
  }
}

provider "kubernetes" {
 host = null_resource.kubeconfig.triggers.host
 token = null_resource.kubeconfig.triggers.token
 cluster_ca_certificate = base64decode(
   null_resource.kubeconfig.triggers.cluster_ca_certificate
 )
}
