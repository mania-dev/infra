resource "scaleway_rdb_instance" "db" {
  name           = "db"
  node_type      = "DB-DEV-S"
  engine         = "PostgreSQL-14"
  is_ha_cluster  = true
  disable_backup = true
  user_name      = var.scaleway_db_user
  password       = var.scaleway_db_password
}
