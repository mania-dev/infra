resource "helm_release" "whosaid" {
  name             = "whosaid"
  create_namespace = true
  namespace        = "whosaid"
  chart            = "whosaid"
  repository       = "abmania-charts"
  version          = "0.1.2"
  values           = [
    file("mania-dev/whosaid.values.yaml")
  ]
}
