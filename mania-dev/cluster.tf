resource "scaleway_k8s_cluster" "mania-dev" {
  name    = "mania-dev"
  version = "1.23.2"
  cni     = "cilium"
}

resource "scaleway_k8s_pool" "mania-dev" {
  cluster_id = scaleway_k8s_cluster.mania-dev.id
  name       = "mania-dev"
  node_type  = "DEV1-M"
  size       = 1
}

resource "null_resource" "kubeconfig" {
  depends_on = [scaleway_k8s_pool.mania-dev] # at least one pool here
  triggers = {
    host                   = scaleway_k8s_cluster.mania-dev.kubeconfig[0].host
    token                  = scaleway_k8s_cluster.mania-dev.kubeconfig[0].token
    cluster_ca_certificate = scaleway_k8s_cluster.mania-dev.kubeconfig[0].cluster_ca_certificate
  }
}
resource "local_file" "kubeconfig" {
  content = scaleway_k8s_cluster.mania-dev.kubeconfig[0].config_file
  filename = pathexpand("~/.kube/mania-dev.yaml")
}
